import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView() {
  const { user } = useContext(UserContext);
  const navigate = useNavigate();
  const { productId } = useParams();
  const [product, setProduct] = useState({});

  useEffect(() => {
    fetch(`https://capstone-2-lansangan-248.onrender.com/products/${productId}`)
      .then(res => res.json())
      .then(data => setProduct(data))
      .catch(error => console.error(error));
  }, [productId]);

  const purchaseProduct = async () => {
    try {
      const response = await fetch(`https://capstone-2-lansangan-248.onrender.com/users/purchase/${productId}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
        body: JSON.stringify({
          productId:productId,
          //userId: user.id,
          //quantity: 1,
        }),
      });

      const data = await response.json();
      if (data === true) {
        Swal.fire({
          title: 'Purchase successful!',
          icon: 'success',
          text: 'Thank you for your order!',
        });

        navigate('/products');
      } else {
        Swal.fire({
          title: 'Purchase successful!',
          icon: 'success',
          text: 'Thank you for your order!',
        });
        navigate('/products')
      }
    } catch (error) {
      console.error('Error occurred while fetching /orders/: ', error);
      Swal.fire({
        title: 'Something went wrong',
        icon: 'error',
        text: 'Please try again.',
      });
    }
  };

  return (
    <Container className="mt-5">
      <Row>
        <Col lg={{ span: 6, offset: 3 }}>
          <Card className="cardHighlight p-3">
            <Card.Body>
              <Card.Title>{product.name}</Card.Title>
              <Card.Subtitle>Description</Card.Subtitle>
              <Card.Text>{product.description}</Card.Text>
              <Card.Subtitle>Price</Card.Subtitle>
              <Card.Text>Php {product.price}</Card.Text>
              {user.id !== null ? (
                <Button variant="primary" onClick={purchaseProduct}>
                  Order Now
                </Button>
              ) : (
                <Link className="btn btn-danger" to="/login">
                  Log in to Order
                </Link>
              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
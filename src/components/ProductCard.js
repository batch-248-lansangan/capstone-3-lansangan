//import useEffect from react
//import { useState, useEffect } from 'react';
/*import { Link } from 'react-router-dom'
import {Row, Col, Card, Button} from 'react-bootstrap';


export default function ProductCard({productProp}) {


  //deconstruct the course properties in their own variable name
  const {name, description, price, _id} = productProp;


  return (
    <Row className="mt-3 mb-3">
      <Col> 
        <Card className="cardHighlight p-3">
          <Card.Body>
            <Card.Title>{name}</Card.Title>
            <Card.Subtitle>Description</Card.Subtitle>
            <Card.Text>
              {description}
            </Card.Text>
            <Card.Subtitle>Price</Card.Subtitle>
            <Card.Text>
              Php {price}
            </Card.Text>
            <Button className="bg-primary" as={Link} to={`/products/${_id}`}>Details</Button>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  )
}*/

import { Link } from 'react-router-dom'
import { Row, Col, Card, Button } from 'react-bootstrap';

export default function ProductCard({ productProp }) {

  // Check if the productProp is defined
  if (!productProp) {
    return null;
  }

  // destructure the course properties in their own variable name
  const { name, description, price, _id } = productProp;

  return (
    <Row className="mt-3 mb-3">
      <Col>
        <Card className="cardHighlight p-3">
          <Card.Body>
            <Card.Title>{name}</Card.Title>
            <Card.Subtitle>Description</Card.Subtitle>
            <Card.Text>{description}</Card.Text>
            <Card.Subtitle>Price</Card.Subtitle>
            <Card.Text>Php {price}</Card.Text>
            <Button className="bg-primary" as={Link} to={`/products/${_id}`}>
              Details
            </Button>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}

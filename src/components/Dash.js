import { useContext, useState, useEffect } from "react";
import {Table, Button} from "react-bootstrap";
import {Navigate, Link} from "react-router-dom";
import UserContext from "../UserContext";

import Swal from "sweetalert2";

export default function Dash(){

  // to validate the user role.
  const {user} = useContext(UserContext);

  //Create allCourses State to contain the courses from the database.
  const [allProducts, setAllProducts] = useState([]);

  //"fetchData()" wherein we can invoke if their is a certain change with the course.
  const fetchData = () =>{
    // Get all products in the database
    fetch(`https://capstone-2-lansangan-248.onrender.com/products/all`,{
      headers:{
        "Authorization": `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      setAllProducts(data.map(product => {
        return(
          <tr key={product._id}>
            <td>{product._id}</td>
            <td>{product.name}</td>
            <td>{product.description}</td>
            <td>{product.price}</td>
            <td>{product.isActive ? "Active" : "Inactive"}</td>
            <td>
              {
                // We use conditional rendering to set which button should be visible based on the course status (active/inactive)
                (product.isActive)
                ? 
                  // A button to change the course status to "Inactive"
                  <Button variant="danger" size="sm" onClick ={() => archive(product._id, product.name)}>Archive</Button>
                :
                  <>
                    {/* A button to change the course status to "Active"*/}
                    <Button variant="success" size="sm" onClick ={() => unarchive(product._id, product.name)}>Unarchive</Button>
                    {/* A button to edit a specific course*/}
                    <Button as={ Link } to={`/editProduct/${product._id}`} variant="secondary" size="sm" className="m-2" >Edit</Button>
                  </>
              }
            </td>
          </tr>
        )
      }))

    })
  }

  //Making the product inactive
  const archive = (productId, productName) =>{
    console.log(productId);
    console.log(productName);

   fetch(`https://capstone-2-lansangan-248.onrender.com/products/archive/${productId}/`,{
      method: "PUT",
      headers:{
        "Content-Type": "application/json",
        "Authorization": `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        isActive: false
      })
    })
    .then(res => res.json())
    .then(data =>{
      console.log(data);

      if(data){
        Swal.fire({
          title: "Archive Succesful!",
          icon: "success",
          text: `${productName} is now inactive.`
        })
        fetchData();
      }
      else{
        Swal.fire({
          title: "Archive Unsuccessful!",
          icon: "error",
          text: `Something went wrong. Please try again later!`
        })
      }
    })
  }

  //Making the product active
  const unarchive = (productId, productName) =>{
    console.log(productId);
    console.log(productName);

    fetch(`https://capstone-2-lansangan-248.onrender.com/products/unarchive/${productId}/`,{
      method: "PUT",
      headers:{
        "Content-Type": "application/json",
        "Authorization": `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        isActive: true
      })
    })
    .then(res => res.json())
    .then(data =>{
      console.log(data);

      if(data){
        Swal.fire({
        title: "Unarchive Succesful!",
        icon: "success",
        text: `${productName} is now active.`
      })
       fetchData();
      }
      else{
        Swal.fire({
        title: "Unarchive Unsuccessful!",
       icon: "error",
          text: `Something went wrong. Please try again later!`
        })
      }
    })
  }

  // To fetch all products in the first render of the page.
  useEffect(()=>{
    // invoke fetchData() to get all .
    fetchData();
  })
  //***[] dependencies are optional

  return(
    (user.isAdmin)
    ?
    <>
      <div className="mt-5 mb-3 text-center">
        <h1>Admin Dashboard</h1>
    
        <Button as={Link} to="/addProduct" variant="primary" size="lg" className="mx-2">Add Product</Button>
        {/*<Button variant="success" size="lg" className="mx-2" disabled>Show Enrollments</Button>*/}
      </div>
      <Table striped bordered hover>
         <thead>
           <tr>
             <th>Product ID</th>
             <th>Product Name</th>
             <th>Description</th>
             <th>Price</th>
             <th>Status</th>
             <th>Action</th>
           </tr>
         </thead>
         <tbody>
           { allProducts }
         </tbody>
       </Table>
    </>
    :
    <Navigate to="/products" />
  )
}


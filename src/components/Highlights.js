import { Row, Col, Card } from "react-bootstrap";

export default function WinHighlights() {
  return (
    <Row className="mt-3 mb-3">
      <Col xs={12} md={4}>
        <Card className="cardHighlight p-3 border-0 shadow">

          <Card.Body>
            <Card.Title>Featured Dish</Card.Title>
            <Card.Text>
              Try our specialty adobo made with the freshest ingredients and our secret blend of spices. Perfectly paired with our specialty craft beer!
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
      <Col xs={12} md={4}>      
        <Card className="cardHighlight p-3 border-0 shadow">

          <Card.Body>
            <Card.Title>Happy Hour</Card.Title>
            <Card.Text>
              Join us every weekday from 4-6pm for discounted beers and appetizers. Bring your friends and enjoy the cozy atmosphere of Win's Kitchen.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
      <Col xs={12} md={4}>
        <Card className="cardHighlight p-3 border-0 shadow">
          <Card.Body>
            <Card.Title>Catering Services</Card.Title>
            <Card.Text>
              Let us cater your next party or event with our special Filipino menu. Contact us for more details and pricing options.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}

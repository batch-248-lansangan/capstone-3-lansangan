import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
// import CourseCard from '../components/CourseCard';


const data = {
	title: "Win's Kitchen",
	content: "Cheers to Filipino flavors and brews!",
	destination: "/products",
	label: "Order now"
}


export default function Home(){

	return (
		<>
			<Banner data={data}/>
    		<Highlights/>
    		{/*<CourseCard/>*/}
		</>
	)
}
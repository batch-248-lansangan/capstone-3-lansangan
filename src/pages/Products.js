import { useEffect, useState, useContext } from 'react';
import { Navigate } from 'react-router-dom';

import ProductCard from '../components/ProductCard';
import UserContext from '../UserContext';

import '../Products.css';

export default function Products() {
  const [products, setProducts] = useState([]);
  const { user } = useContext(UserContext);

  useEffect(() => {
    fetch(`https://capstone-2-lansangan-248.onrender.com/products/`)
      .then((res) => res.json())
      .then((data) => {

      	const productArr = data.map((product) => {
      	  return <ProductCard productProp={product} key={product._id} />;
      	});
      	
        setProducts(productArr);
      });
  }, []);

  return (
  		(user.isAdmin) ?
  		<Navigate to="/admin"/>
  		:
    <>
      <h1>Menu</h1>
      {products}
    </>
  );
}
